// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// Specify that when we browse to "/" with a GET request, render the
// questionnaire/questionnaire-form view

app.get('/', function (req, res) {

    res.render("questionnaire/questionnaire-form", { pageTitle: "Ex 02 Form" });
});
app.get('/getForm', function (req, res) {
    var data = {
     
        pageTitle: "Echo Service Submission",
        useGet: true
        };
    res.render("questionnaire/questionnaire-form", data);
});

app.get('/submission', function(req, res) {
    console.log("submission action using GET request");
    var data = {
        pageTitle: "Echo Service Submission",
        entries: req.query
        };
        res.render("echo-service/echo-service-submission", data);
});

// TODO Step 1. Add a route handler for POST requests to "/submission".
app.post('/submission', function (req, res) {
    console.log("submission action using POST request"); 
    var data = {
        pageTitle: "Echo Service Submission",
        entries: req.body
        };
        res.render("echo-service/echo-service-submission", data);
});


// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});